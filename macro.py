# Std library
import ctypes
import time
# Third party
import win32api
import win32con
import win32clipboard


# Mouse functions
def mouse_mv(x, y):
	ctypes.windll.user32.BlockInput(True)
	win32api.SetCursorPos((x-2, y-2))
	time.sleep(1)
	win32api.SetCursorPos((x, y))
	time.sleep(0.5)
	ctypes.windll.user32.BlockInput(False)

def mouse_left_click(x = None, y = None):
	ctypes.windll.user32.BlockInput(True)
	if x and y:
		mouse_mv(x, y)
	else:
		x, y = win32api.GetCursorPos()
	win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)
	win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)
	time.sleep(0.5)
	ctypes.windll.user32.BlockInput(False)
	
def mouse_right_click(x = None, y = None):
	ctypes.windll.user32.BlockInput(True)
	if x and y:
		mouse_mv(x, y)
	else:
		x, y = win32api.GetCursorPos()
	win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN, x, y, 0, 0)
	win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP, x, y, 0, 0)
	time.sleep(0.5)
	ctypes.windll.user32.BlockInput(False)


# Keyboard functions
def kb_type(text):
	ctypes.windll.user32.BlockInput(True)
	for char in str(text):
		if char.isupper():
			win32api.keybd_event(win32con.VK_SHIFT, 0, 0, 0)
			win32api.keybd_event(win32api.VkKeyScanEx(char, 0), 0, 0, 0)
			win32api.keybd_event(win32api.VkKeyScanEx(char, 0), 0, win32con.KEYEVENTF_KEYUP, 0)
			win32api.keybd_event(win32con.VK_SHIFT, 0, win32con.KEYEVENTF_KEYUP, 0)
		else:
			win32api.keybd_event(win32api.VkKeyScanEx(char, 0), 0, 0, 0)
			win32api.keybd_event(win32api.VkKeyScanEx(char, 0), 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.25)
	ctypes.windll.user32.BlockInput(False)

def kb_up():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_UP, 0, 0, 0)
	win32api.keybd_event(win32con.VK_UP, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.25)
	ctypes.windll.user32.BlockInput(False)

def kb_down():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_DOWN, 0, 0, 0)
	win32api.keybd_event(win32con.VK_DOWN, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.25)
	ctypes.windll.user32.BlockInput(False)

def kb_copy():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_CONTROL, 0, 0, 0)
	win32api.keybd_event(win32api.VkKeyScanEx('c', 0), 0, 0, 0)
	time.sleep(0.25)
	win32api.keybd_event(win32api.VkKeyScanEx('c', 0), 0, win32con.KEYEVENTF_KEYUP, 0)
	win32api.keybd_event(win32con.VK_CONTROL, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.5)
	ctypes.windll.user32.BlockInput(False)
	
def kb_paste():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_CONTROL, 0, 0, 0)
	win32api.keybd_event(win32api.VkKeyScanEx('v', 0), 0, 0, 0)
	time.sleep(0.25)
	win32api.keybd_event(win32api.VkKeyScanEx('v', 0), 0, win32con.KEYEVENTF_KEYUP, 0)
	win32api.keybd_event(win32con.VK_CONTROL, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.25)
	ctypes.windll.user32.BlockInput(False)
	
def kb_enter():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_RETURN, 0, 0, 0)
	win32api.keybd_event(win32con.VK_RETURN, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.5)
	ctypes.windll.user32.BlockInput(False)

def kb_delete_all():
	ctypes.windll.user32.BlockInput(True)
	win32api.keybd_event(win32con.VK_CONTROL, 0, 0, 0)
	win32api.keybd_event(win32api.VkKeyScanEx('a', 0), 0, 0, 0)
	time.sleep(0.25)
	win32api.keybd_event(win32api.VkKeyScanEx('a', 0), 0, win32con.KEYEVENTF_KEYUP, 0)
	win32api.keybd_event(win32con.VK_CONTROL, 0, win32con.KEYEVENTF_KEYUP, 0)
	win32api.keybd_event(win32con.VK_DELETE, 0, 0, 0)
	win32api.keybd_event(win32con.VK_DELETE, 0, win32con.KEYEVENTF_KEYUP, 0)
	time.sleep(0.25)
	ctypes.windll.user32.BlockInput(False)

def kb_wait_left_alt():
	win32api.GetAsyncKeyState(win32con.VK_LMENU)
	while True:
		if win32api.GetAsyncKeyState(win32con.VK_LMENU):
			time.sleep(0.5)
			break
		else:
			time.sleep(0.25)


# Clipboard functions
def clipboard_set(text):
	win32clipboard.OpenClipboard()
	win32clipboard.EmptyClipboard()
	win32clipboard.SetClipboardText(str(text))
	win32clipboard.CloseClipboard()
	time.sleep(0.25)

def clipboard_get():
	win32clipboard.OpenClipboard()
	data = None
	if win32clipboard.IsClipboardFormatAvailable(win32clipboard.CF_UNICODETEXT):
		data = win32clipboard.GetClipboardData(win32clipboard.CF_UNICODETEXT)
	elif win32clipboard.IsClipboardFormatAvailable(win32clipboard.CF_TEXT):
		data = win32clipboard.GetClipboardData(win32clipboard.CF_TEXT)
	elif win32clipboard.IsClipboardFormatAvailable(win32clipboard.CF_OEMTEXT):
		data = win32clipboard.GetClipboardData(win32clipboard.CF_OEMTEXT)
	win32clipboard.CloseClipboard()
	return data

def clipboard_clr():
	win32clipboard.OpenClipboard()
	win32clipboard.EmptyClipboard()
	win32clipboard.CloseClipboard()
	time.sleep(0.25)
