# Std library
import ConfigParser
import tabulate
import time
# Local
import macro


class Manufacture(object):
	def __init__(self):
		self.config = ConfigParser.ConfigParser()
		self.items = {}
		self.array_num = None
	
	def readConfig(self, file = 'config.ini'):
		# Read in EVE UI co-ordinates from config file
		self.config.readfp(open(file))
	
	def readItems(self, file = 'build.dat'):
		# Read in items to be produced from file
		with open(file, 'r') as f:
			for line in f:
				line = line.rstrip().split('\t')
				item = line[0]
				num_runs = int(line[1].replace(',', ''))
				if num_runs and item == 'Ion Thruster':
					print "Warning: Ion Thruster jobs cannot be installed automatically"
				elif num_runs:
					self.items[item] = num_runs
	
	def printItems(self):
		# Print items to be produced
		table = []
		for item, num_runs in sorted(self.items.iteritems()):
			table.append([item, num_runs])
		print "\n", tabulate.tabulate(table, headers=['Item', 'Quantity']), "\n\n"
	
	def getArrayNum(self):
		# Ask for array number
		while True:
			try:
				self.array_num = int(raw_input("Enter the array number: "))
				break
			except ValueError:
				print "Error: Number was not an integer"
		
		# Make sure corp hangar is open before continuing
		raw_input("Press enter to continue (make sure corp hangar is open) ")
	
	def installJobs(self):
		# Initialize count
		count = 0
		
		# Loop through the items and install jobs
		for item, num_runs in sorted(self.items.iteritems()):
			# Output current item
			if not count:
				print
			print "Installing %s..." % item
			
			# Set focus to EVE window
			macro.mouse_left_click(self.config.getint('eve', 'x'), self.config.getint('eve', 'y'))
			
			# Select BPO and open manufacturing dialogue
			macro.mouse_left_click(self.config.getint('filter', 'x'), self.config.getint('filter', 'y'))
			macro.kb_delete_all()
			macro.clipboard_set(item)
			macro.kb_paste()
			macro.mouse_right_click(self.config.getint('bpo', 'x'), self.config.getint('bpo', 'y'))
			macro.mouse_left_click(self.config.getint('bpo', 'x') + 30, self.config.getint('bpo', 'y') + 170)
			
			# Select array slot to use
			macro.mouse_left_click(self.config.getint('installation', 'x'), self.config.getint('installation', 'y'))
			if not count:
				macro.mouse_left_click(self.config.getint('range', 'x'), self.config.getint('range', 'y'))
				macro.mouse_left_click(self.config.getint('range', 'x'), self.config.getint('range', 'y') + 60)	# Solar system
				macro.mouse_left_click(self.config.getint('range', 'x') + 90, self.config.getint('range', 'y'))
				macro.mouse_left_click(self.config.getint('range', 'x') + 90, self.config.getint('range', 'y') + 60)	# Corporation
			macro.mouse_left_click(self.config.getint('range', 'x'), self.config.getint('range', 'y') + 40 + (self.array_num-1)*20)	# Array
			time.sleep(0.5)
			macro.mouse_left_click(self.config.getint('range', 'x'), self.config.getint('range', 'y') + 330)	# Slot
			macro.kb_enter()
			
			# Enter number of runs to produce
			macro.mouse_left_click(self.config.getint('runs', 'x'), self.config.getint('runs', 'y'))
			macro.kb_delete_all()
			macro.clipboard_set(num_runs)
			macro.kb_paste()
			macro.kb_enter()
			
			# Wait for left alt button to be pressed before accepting quote
			macro.kb_wait_left_alt()
			macro.kb_enter()
			
			# Wait for client to update	
			if not count:
				time.sleep(5)
			else:
				time.sleep(3)
			
			# Update count
			count += 1
	
	def run(self):
		# Get necessary data and install jobs
		self.readConfig()
		self.readItems()
		self.printItems()
		self.getArrayNum()
		self.installJobs()


if __name__ == '__main__':
	# Check that the right components are being built
	raw_input("Press enter to continue (make sure components.dat is up-to-date) ")
	while True:
		# Create new manufacturing instance and install jobs
		manufacturing = Manufacture()
		manufacturing.run()
		
		# Enable user to restart manufacturing process
		opt = raw_input("\nEnter 'r' to restart or leave clear to quit ")
		if opt != 'r' and opt != 'R':
			break
