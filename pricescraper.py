# Std library
import ConfigParser
import datetime
import shutil
# Third party
from reverence import blue


class PriceScraper(object):
	def __init__(self, config_file = 'config.ini'):
		# Read in configuration file
		self._config = ConfigParser.ConfigParser()
		self._config.readfp(open(config_file))
		
		# Initialize Reverence cache manager
		eve = blue.EVE(self._config.get('eve', 'path'))
		self._cachemgr = eve.getcachemgr()
		
		# Materials
		self.materials = {}
		self.materials['name'] = 'Materials'
		self.materials[16670] = 'Crystalline Carbonide'
		self.materials[17317] = 'Fermionic Condensates'
		self.materials[16673] = 'Fernite Carbide'
		self.materials[16683] = 'Ferrogel'
		self.materials[16679] = 'Fullerides'
		self.materials[16682] = 'Hypersynaptic Fibers'
		self.materials[16681] = 'Nanotransistors'
		self.materials[16680] = 'Phenolic Composites'
		self.materials[16678] = 'Sylramic Fibers'
		self.materials[16671] = 'Titanium Carbide'
		self.materials[16672] = 'Tungsten Carbide'
		self.materials[33359] = 'Photonic Metamaterials'
		self.materials[33362] = 'Nonlinear Metamaterials'
		self.materials[33360] = 'Terahertz Metamaterials'
		self.materials[33361] = 'Plasmonic Metamaterials'
		
		# Amarr components
		self.amarr = {}
		self.amarr['name'] = 'Amarr Components'
		self.amarr[11549] = 'Antimatter Reactor Unit'
		self.amarr[11694] = 'EM Pulse Generator'
		self.amarr[11532] = 'Fusion Thruster'
		self.amarr[11689] = 'Laser Focusing Crystals'
		self.amarr[11557] = 'Linear Shield Emitter'
		self.amarr[11539] = 'Nanoelectrical Microprocessor'
		self.amarr[11537] = 'Radar Sensor Cluster'
		self.amarr[11554] = 'Tesseract Capacitor Unit'
		self.amarr[11543] = 'Tungsten Carbide Armor Plate'
		
		# Caldari components
		self.caldari = {}
		self.caldari['name'] = 'Caldari Components'
		self.caldari[11534] = 'Gravimetric Sensor Cluster'
		self.caldari[11693] = 'Graviton Pulse Generator'
		self.caldari[11550] = 'Graviton Reactor Unit'
		self.caldari[11533] = 'Magpulse Thruster'
		self.caldari[11540] = 'Quantum Microprocessor'
		self.caldari[11552] = 'Scalar Capacitor Unit'
		self.caldari[11690] = 'Superconductor Rails'
		self.caldari[11558] = 'Sustained Shield Emitter'
		self.caldari[11544] = 'Titanium Diborite Armor Plate'
		
		# Gallente components
		self.gallente = {}
		self.gallente['name'] = 'Gallente Components'
		self.gallente[11545] = 'Crystalline Carbonide Armor Plate'
		self.gallente[11547] = 'Fusion Reactor Unit'
		self.gallente[11531] = 'Ion Thruster'
		self.gallente[11535] = 'Magnetometric Sensor Cluster'
		self.gallente[11553] = 'Oscillator Capacitor Unit'
		self.gallente[11688] = 'Particle Accelerator Unit'
		self.gallente[11541] = 'Photon Microprocessor'
		self.gallente[11695] = 'Plasma Pulse Generator'
		self.gallente[11556] = 'Pulse Shield Emitter'
		
		# Minmatar components
		self.minmatar = {}
		self.minmatar['name'] = 'Minmatar Components'
		self.minmatar[11555] = 'Deflection Shield Emitter'
		self.minmatar[11551] = 'Electrolytic Capacitor Unit'
		self.minmatar[11542] = 'Fernite Carbide Composite Armor Plate'
		self.minmatar[11536] = 'Ladar Sensor Cluster'
		self.minmatar[11538] = 'Nanomechanical Microprocessor'
		self.minmatar[11692] = 'Nuclear Pulse Generator'
		self.minmatar[11548] = 'Nuclear Reactor Unit'
		self.minmatar[11530] = 'Plasma Thruster'
		self.minmatar[11691] = 'Thermonuclear Trigger Unit'
	
	def getMinPrices(self, items, f):
		# Notify
		print "Processing %s" % items['name']
		
		# Output HTML table header
		f.write('<table>\n')
		f.write('<tr><th>%s</th><th>Min Sell</th></tr>\n' % items['name'])
		
		del items['name']
		for typeID in sorted(items, key=items.get):
			# Cache file key for pickling
			key = ('marketProxy', 'GetOrders', 10000002L, typeID)	# The Forge region ID
			
			# Scrape cache to get minimum sell price
			price = []
			if self._cachemgr.FindCacheFile(key):
				obj = self._cachemgr.LoadCachedMethodCall(key)
				for entry in obj['lret'][0]:	# lret[0] contains sell orders, lret[1] contains buy orders
					if entry['stationID'] == 60003760:	# Jita IV-4 station ID
						price.append(entry['price'])
				if price:
					price = min(price)
				else:
					print "\tCould not find price for: %s" % items[typeID]
			
			# Output data
			f.write('<tr><td>%s</td><td>%.2f</td></tr>\n' % (items[typeID], price or 0))
			
		# Ouput HTML table footer
		f.write('</table>\n')
	
	def run(self):
		# Create output file with HTML header and footers and run cache scraper
		out_file = self._config.get('eve', 'prices_out')
		with open(out_file, 'w') as f:
			f.write('<html>\n')
			f.write('<body>\n')
			print
			self.getMinPrices(self.materials, f)
			self.getMinPrices(self.amarr, f)
			self.getMinPrices(self.caldari, f)
			self.getMinPrices(self.gallente, f)
			self.getMinPrices(self.minmatar, f)
			f.write('</body>\n')
			f.write('</html>\n')
		
		# Make copy of file
		dest = out_file.replace('.html', '_%s.html' % datetime.datetime.now().strftime("%Y-%m-%d_%H-%M"))
		shutil.copyfile(out_file, dest)


if __name__ == '__main__':
	# Initialize price scraper and scrape cache for market data
	scraper = PriceScraper()
	scraper.run()
	
	# Wait for user before quitting	
	raw_input("\nPress enter to quit ")
