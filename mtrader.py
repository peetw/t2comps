# Std library
import ConfigParser
import logging
import random
import re
import tabulate
import time
# Third party
from reverence import blue
# Local
import macro


class Order(object):
	def __init__(self, type_name, quantity, price, station, region, expiry, config, configmgr, cachemgr):
		# Data obtained from the EVE client
		self.type_name = type_name
		self.quantity = quantity
		self.price = price
		self.station = station
		self.region = region
		self.expiry = expiry
		
		# EVE UI coordinates
		self._config = config

		# Reverence cache managers
		self._configmgr = configmgr
		self._cachemgr = cachemgr

		# Hash to identify orders
		self.hash = hash((type_name, quantity['entered'], station))
		
		# Derived data and other variables
		self.issued = self._getIssuedTime()
		self.next_check = self._getNextCheck()
		self.past_last_order = False
		self.new_price = price
		self.x = 0
		self.y = 0

	def _getIssuedTime(self):
		t = int(time.time())
		t -= (89 - self.expiry['days']) * 24*60*60
		t -= (23 - self.expiry['hours']) * 60*60
		t -= (59 - self.expiry['mins']) * 60
		t -= (60 - self.expiry['secs'])
		return t

	def _getNextCheck(self):
		t = int(time.time())
		diff = t - self.issued
		if diff < 5*60:
			return t + (5*60 - diff)
		else:
			return t

	def isPastLastOrder(self):
		return self.past_last_order

	def isModifiable(self):
		# Assumes 90 day orders
		if self.expiry['days'] < 89 or self.expiry['hours'] < 23 or self.expiry['mins'] < 55:
			logging.debug("Order for %s can be modified", self.type_name)
			return True
		else:
			logging.debug("Order for %s cannot be modified", self.type_name)
			return False

	def needsUpdating(self):
		# Check in-game market
 		macro.mouse_right_click(self.x, self.y)
 		macro.mouse_left_click(self.x + 20, self.y + 10)
 		time.sleep(0.25+random.random())
 		macro.mouse_left_click(self._config.getint('view_market', 'x'), self._config.getint('view_market', 'y'))
 		time.sleep(6+random.random()*4)
		
		# Get typeIDs and create key for cache access
		type_id = self._configmgr.invtypes.IndexedBy('typeName').Get(self.type_name).typeID
		station_id = self._configmgr.stations.IndexedBy('stationName').Get(self.station).stationID
		region_id = long(self._configmgr.evelocations.IndexedBy('locationName').Get(self.region).locationID)
		key = ('marketProxy', 'GetOrders', region_id, type_id)
		
		# Parse cachefile and get min sell price in same station as order
		if self._cachemgr.FindCacheFile(key):
			price = []
			obj = self._cachemgr.LoadCachedMethodCall(key)
			for entry in obj['lret'][0]:	# lret[0] contains sell orders, lret[1] contains buy orders
				if entry['stationID'] == station_id:
					price.append(entry['price'])
			if price:
				min_price = min(price)
				if min_price < self.price:
					self.new_price = min_price - random.randrange(1,3)/100.0
					logging.debug("Order for %s needs to be updated", self.type_name)
					return True
				else:
					logging.debug("Order for %s is already cheapest on market", self.type_name)
			else:
				logging.warning("No matching orders found in cache file")
		else:
			logging.warning("Could not find cache file for %s", self.type_name)
		return False

	def update(self):
		# Update price in-game
		macro.mouse_left_click(self._config.getint('eve', 'x'), self._config.getint('eve', 'y'))
		macro.mouse_left_click(self._config.getint('price', 'x'), self._config.getint('price', 'y'))
		macro.kb_delete_all()
		macro.clipboard_set(self.new_price)
		macro.kb_paste()
		macro.kb_enter()
		macro.kb_enter()
		
		# Update order info
		logging.debug("Order for %s updated (old price = %.2f; new price = %.2f)", self.type_name, self.price, self.new_price)
		self.price = self.new_price
		self.issued = int(time.time())
		self.next_check = self.issued + (5*60)


class OrderManager(object):
	def __init__(self, config_file = 'config.ini'):
		# Read in configuration file
		self._config = ConfigParser.ConfigParser()
		self._config.readfp(open(config_file))
		
		# Initialize Reverence cache managers
		eve = blue.EVE(self._config.get('eve', 'path'))
		self._configmgr = eve.getconfigmgr()
		self._cachemgr = eve.getcachemgr()
		
		# Initialize order list
		self._orders = []
		
		# Initialize logging
		logging.basicConfig(filename='mtrader.log', filemode='w', level=logging.DEBUG, format='%(asctime)s %(levelname)-8s %(message)s', datefmt='%Y-%d-%m %H:%M:%S')
		logging.info("Initializing OrderManager using '%s'", config_file)

	def _parseQuantity(self, str):
		q = {}
		q['entered'] = int(re.search(r"/([0-9]+)", str).group(1))
		q['remaining'] = int(re.search(r"([0-9]+)/", str).group(1))
		return q

	def _parseExpiry(self, str):
		t = {'days': 0, 'hours': 0, 'mins': 0, 'secs': 0}
		search_found = re.search(r"([0-9]+)d", str)
		if search_found:
			t['days'] = int(search_found.group(1))
		search_found = re.search(r"([0-9]+)h", str)
		if search_found:
			t['hours'] = int(search_found.group(1))
		search_found = re.search(r"([0-9]+)m", str)
		if search_found:
			t['mins'] = int(search_found.group(1))
		search_found = re.search(r"([0-9]+)s", str)
		if search_found:
			t['secs'] = int(search_found.group(1))
		return t

	def _parseRow(self, row = None):
		if row:
			row_split = row.split('\t')
			if len(row_split) == 6:
				type_name = row_split[0]
				quantity = self._parseQuantity(row_split[1].replace(',', ''))
				price = float(row_split[2].split(' ')[0].replace(',', ''))
				expiry = self._parseExpiry(row_split[3])
				station = row_split[4]
				region = row_split[5]
				return Order(type_name, quantity, price, station, region, expiry, self._config, self._configmgr, self._cachemgr)
		return None

	def getNextOrder(self):
		# Copy order row to clipboard and parse
		macro.mouse_left_click(self._config.getint('eve', 'x'), self._config.getint('eve', 'y'))
		time.sleep(0.25+random.random()*0.25)
		macro.mouse_left_click(self._config.getint('my_orders', 'x'), self._config.getint('my_orders', 'y'))
		time.sleep(0.25+random.random()*0.25)
		macro.mouse_left_click(self._config.getint('my_orders', 'x'), self._config.getint('my_orders', 'y') + (self.numOrders()*2 + 4)*10)
		time.sleep(0.25+random.random()*0.25)
		if self.numOrders():
			macro.kb_up()
			macro.kb_down()
		macro.clipboard_clr()
		time.sleep(0.25+random.random()*0.25)
		macro.kb_copy()
		macro.kb_copy()
		row = macro.clipboard_get()
		logging.debug("Copied row: %s", row)
		order = self._parseRow(row)
		
		# Add order to list of active orders
		if order:
			order.x = self._config.getint('my_orders', 'x')
			order.y = self._config.getint('my_orders', 'y') + (self.numOrders()*2 + 4)*10
			self._orders.append(order)
			# Check if same order as previous one (i.e. end of active orders)
			if self.numOrders() > 1 and self._orders[self.numOrders()-2].hash == order.hash:
				order.past_last_order = True
			return order
		else:
			return None

	def numOrders(self):
		return len(self._orders)

	def printOrders(self):
		# Populate table data
		del self._orders[-1]
		table = []
		hashes = []
		for order in self._orders:
			key = hash((order.type_name, order.quantity['entered'], order.price, order.station))
			if not key in hashes:
				hashes.append(key)
				table.append([order.type_name, order.quantity['remaining'], order.price, order.station, time.ctime(order.issued)])
		
		# Output table data in pretty format
		print "\nActive market orders (%s):\n" % time.ctime()
		print tabulate.tabulate(table, headers=['Item', 'Quantity', 'Price', 'Station', 'Issued']), "\n"

	def sleepUntilNextCheck(self):
		# Get list of next check times
		next_check = []
		for order in self._orders:
			next_check.append(order.next_check)
		
		# Calculate time to sleep until next order is modifiable again
		diff = min(next_check) - int(time.time())
		if diff > 0:
			time.sleep(diff)
		
		# Reset list of active orders
		self._orders = []

	# Do not run program near DT (between 09:45 and 11:30 UTC) 
	def timeIsOk(self):
		utc_time = time.strftime("%H:%M", time.gmtime())
		if utc_time >= '09:45' and utc_time <= '11:30':
			logging.info("Market trader is quitting: Time is between 09:45 and 11:30 UTC")
			return False
		else:
			return True

	def run(self):
		try:
			# Loop until all orders completed or time is near DT
			print "\nRunning mtrader..."
			while self.timeIsOk():
				order = self.getNextOrder()
			
				if not order:
					logging.info("Market trader is quitting: No active orders found")
					break
				elif order.isPastLastOrder():
					self.printOrders()
					self.sleepUntilNextCheck()
				elif order.isModifiable() and order.needsUpdating():
					order.update()
		except Exception as e:
			print "Error encountered: Check logs"
			logging.exception(e)


if __name__ == '__main__':
	# Initialize order manager and monitor orders
	ordermgr = OrderManager()
	ordermgr.run()
	
	# Wait for user before quitting	
	raw_input("\nPress enter to quit ")
