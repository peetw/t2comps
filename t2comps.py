# Std library
import ConfigParser
# Local
import manufacture
import mtrader
import pricescraper


def create_config():
	# Initialize config parser and add required sections
	config = ConfigParser.RawConfigParser()
	config.add_section('eve')
	config.add_section('my_orders')
	config.add_section('view_market')
	config.add_section('price')
	config.add_section('filter')
	config.add_section('bpo')
	config.add_section('installation')
	config.add_section('range')
	config.add_section('runs')

	# Add blank options
	config.set('eve', 'path', 'C:\Program Files\EVE')
	config.set('eve', 'prices_out', 'prices.html')
	for section in config.sections():
		config.set(section, 'x', '')
		config.set(section, 'y', '')
	
	with open('config_default.ini', 'w') as f:
		config.write(f)


if __name__ == '__main__':
	# Create menu and display it
	menu = {}
	menu['1'] = "Create template config file"
	menu['2'] = "Scrape market data from cache"
	menu['3'] = "Install manufacturing jobs"
	menu['4'] = "Run market trader"
	
	print "\n--------\nOptions\n--------"
	for entry in sorted(menu):
		print "  ", str(entry) + ".", menu[entry]
	
	# Get menu option and perform selected task
	while True:
		selection = raw_input("\nSelect an option: ")
		
		if selection == '1':
			# Create template configuration file
			create_config()
			break
		elif selection == '2':
			# Initialize price scraper and scrape cache for market data
			scraper = pricescraper.PriceScraper()
			scraper.run()
			break
		elif selection == '3':
			# Create new manufacturing instance and install jobs
			manufacturing = manufacture.Manufacture()
			manufacturing.run()
			break
		elif selection == '4':
			# Initialize order manager and monitor orders
			ordermgr = mtrader.OrderManager()
			ordermgr.run()
			break
		else:
			print "Error: Unknown option selected!"
	
	# Wait for user before quitting
	raw_input("\nPress enter to quit ")
